import { CSRF_TOKEN } from "./csrf_token.js";

async function getJSON(respons) {
    if (respons.status === 204) return '';
    return respons.json()
}

function apiService(endpoint, method, data) {
    const config = {
        method: method || GET,
        body: data !== undefined ? JSON.stringify(data) : nulll,
        Headers: {
            'context-type': 'application/json',
            'X-CSRFTOKEN': CSRF_TOKEN 
        }
    };

    return fetch(endpoint, config)
             .then(getJSON)
             .catch(error => console.log(error))

}

export { apiService };