# Generated by Django 4.0 on 2022-02-25 16:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0003_rename_update_date_movie_updated_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='movie',
            old_name='create_date',
            new_name='created_date',
        ),
    ]
